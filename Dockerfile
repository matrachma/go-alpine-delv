# Start from the latest golang base image
FROM golang:1.13.5-alpine3.10

# Add Maintainer Info
LABEL maintainer="Hikmat Rachmatia <hikmat@metroney.com>"

# Compile Delve
RUN \
        # DNS problem workaround
        # https://github.com/gliderlabs/docker-alpine/issues/386
        printf "nameserver 8.8.8.8\nnameserver 9.9.9.9\nnameserver 1.1.1.1" > /etc/resolv.conf \
        \
        && cd /home/ && apk add --no-cache git && go get github.com/go-delve/delve/cmd/dlv
